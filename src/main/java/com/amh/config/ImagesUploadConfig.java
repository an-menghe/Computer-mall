package com.amh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//处理器拦截器的注册
@Configuration //加载当前的拦截器并进行注册
public class ImagesUploadConfig implements WebMvcConfigurer {
    /**
     * 解决 解决SpringBoot图片上传需重启服务器才能显示问题
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        System.out.println("配置文件已经生效");
        registry.addResourceHandler("/upload/**").addResourceLocations("file:E:/电脑商城/store/src/main/resources/static/upload/");
    }
}
