package com.amh.controlle;

import com.amh.controlle.ex.*;
import com.amh.service.ex.*;
import com.amh.util.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpSession;

//控制器类的基类
public class BaseController {
    //操作成功的状态码
    public static final int OK = 200;

    //请求处理方法，这个方法的返回值就是需要传递给前端的数据
    //自动将异常对象传递给此方法的参数列表上
    // 当项目中产生了异常，被统一拦截到此方法中，这个方法此时就充当的是请求处理方法。返回值直接返回给到前端
    @ExceptionHandler({ServiceException.class,FileUploadException.class}) //统一处理抛出的异常
    public JsonResult<Void> handleException(Throwable e) {
        JsonResult<Void> result = new JsonResult<>();
        if (e instanceof UsernameDuplicatedException) {
            result.setState(4000);
            result.setMessage(e.getMessage());
        }else if (e instanceof UserNotFoundException) {
            result.setState(4001);
            result.setMessage(e.getMessage());
        }else if (e instanceof PasswordNotMatchException) {
            result.setState(4002);
            result.setMessage(e.getMessage());
        }else if (e instanceof AddressCountLimitException) {
            result.setState(4003);
            result.setMessage(e.getMessage());
        }else if (e instanceof AddressNotFoundException) {
            result.setState(4004);
            result.setMessage(e.getMessage());
        } else if (e instanceof AccessDeniedException) {
            result.setState(4005);
            result.setMessage(e.getMessage());
        }else if (e instanceof ProductNotFoundException) {
            result.setState(4006);
            result.setMessage(e.getMessage());
        }else if (e instanceof CartNotFoundException) {
            result.setState(4007);
            result.setMessage(e.getMessage());
        }else if (e instanceof InsertException) {
            result.setState(5000);
            result.setMessage(e.getMessage());
        }else if (e instanceof UpdateException) {
            result.setState(5001);
            result.setMessage(e.getMessage());
        }else if (e instanceof DeleteException) {
            result.setState(5002);
            result.setMessage(e.getMessage());
        }else if (e instanceof FileEmptyException) {
            result.setState(6000);
            result.setMessage(e.getMessage());
        }else if (e instanceof FileSizeException) {
            result.setState(6001);
            result.setMessage(e.getMessage());
        }else if (e instanceof FileTypeException) {
            result.setState(6002);
            result.setMessage(e.getMessage());
        }else if (e instanceof FileStateException) {
            result.setState(6003);
            result.setMessage(e.getMessage());
        }else if (e instanceof FileUploadIOException) {
            result.setState(6004);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 获取session对象的uid
     * @param session session 对象
     * @return 当前登录的用户uid的值
     */
    protected final Integer getuidFromSession(HttpSession session){
        return Integer.valueOf(session.getAttribute("uid").toString());
    }

    /**
     * 获取当前登录用户的username
     * @param session session 对象
     * @return 当前登录用户的用户名
     *
     * 在实现类中重写父类的toString()，不是句柄信息的输出
     */
    protected final String getUsernameFromSession(HttpSession session){
        return session.getAttribute("username").toString();
    }




}
