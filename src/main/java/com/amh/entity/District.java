package com.amh.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/** 省/市/区数据的实体类 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class District extends BaseEntity implements Serializable {
    private Integer id;
    private String parent;
    private String code;
    private String name;

    // Generate: Getter and Setter、Generate hashCode() and equals()、toString()
}
