package com.amh.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** 购物车数据的Value Object类 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CartVO {
    private Integer cid;
    private Integer uid;
    private Integer pid;
    private Long price;
    private Integer num;
    private String title;
    private Long realPrice;
    private String image;

}
