package com.amh.service;

import com.amh.entity.Address;

import java.util.List;

//收获地址业务层接口
public interface IAddressService {

   void addNewAddress(Integer uid, String username, Address address);

   List<Address> getByUid(Integer uid);

   /**
    * 设置默认收货地址
    * @param aid 收货地址id
    * @param uid 归属的用户id
    * @param username 当前登录的用户名
    */
   void setDefault(Integer aid, Integer uid, String username);

   /**
    * 删除收货地址
    * @param aid 收货地址id
    * @param uid 归属的用户id
    * @param username 当前登录的用户名
    */
   void delete(Integer aid, Integer uid, String username);

   /**
    * 根据aid查询地址信息
    * @param aid 地址id
    */
   Address selectAddressAid(Integer aid);

   /**
    * 根据aid更改用户收货地址
    * @param username 用户名称
    * @param address 地址信息
    */
   void updateAddressByAid(Integer uid,String username,Address address);

   /**
    * 根据收货地址数据的id，查询收货地址详情
    * @param aid 收货地址id
    * @param uid 归属的用户id
    * @return 匹配的收货地址详情
    */
   Address getByAid(Integer aid, Integer uid);
}
