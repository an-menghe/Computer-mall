package com.amh.service;

import com.amh.entity.District;

import java.util.List;

public interface IDistrictService {
    /**
     * 根据用户的父代号查询区域的信息（省市区）
     * @param parent 父代码
     * @return 多个区域的信息
     */
    List<District> getByParent(String parent);

    String getNameByCode(String code);
}
