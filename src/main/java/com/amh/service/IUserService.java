package com.amh.service;

import com.amh.entity.User;
import org.springframework.stereotype.Service;

//用户模块业务层接口
public interface IUserService {
    /**
     * 用户注册方法
     * @param user 用户的数据对象
     */
    void reg(User user);

    /**
     * 用户登录功能
     * @param username 用户名
     * @param password 密码
     * @return 当前匹配的用户数据，如果没有返回null值
     */
    User login(String username,String password);


    /**
     * 更改用户密码
     * @param uid
     * @param username
     * @param olePassword
     * @param newPassword
     */
    void changePassword(Integer uid,
                        String username,
                        String olePassword,
                        String newPassword);

    /**
     * 根据用户的id 查询用户的数据
     * @param uid 用户id
     * @return 用户的数据
     */
    User getByUid(Integer uid);

    /**、
     * 更新用户的数据操作
     * @param uid 用户的id
     * @param username 用户名
     * @param user 用户对象的数据
     */
    void changeInfo(Integer uid,String username,User user);

    /**
     * 修改用户的头像
     * @param uid 用户id
     * @param avatar 用户头像路径
     * @param username 用户名称
     */
    void changeAvatar(Integer uid,
                      String avatar,
                      String username);
}
