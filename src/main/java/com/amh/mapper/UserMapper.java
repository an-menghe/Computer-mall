package com.amh.mapper;

import com.amh.entity.User;
import org.apache.ibatis.annotations.Param;

import javax.swing.*;
import java.util.Date;

//用户模块的持久层的接口
public interface UserMapper {
    /**
     * 插入用户的数据
     * @param user 用户的数据
     * @return 受影响的行数
     */
    Integer insert(User user);

    /**
     * 根据用户名查询用户的数据
     * @param username 用户名
     * @return 如果找到对应名称 返回该用户数据  ，如果没有找到则返回null值
     */
    User findByUsername(String username);

    /**
     * 根据用户的uid来修改用户密码
     * @param uid uid  用户id
     * @param password 用户输入的新密码
     * @param modifiedUser 表示修改的执行者
     * @param modifiedTime 表示修改数据的时间
     * @return 返回值为受影响的行数
     */
   Integer updatePasswordByUid(Integer uid,
                               String password,
                               String modifiedUser,
                               Date modifiedTime);

    /**
     * 根据用户id查询用户的数据
     * @param uid 用户id
     * @return 找到返回对象，反之返回null值
     */
   User findByUid(Integer uid);

    /**
     * 更新用户的数据信息
     * @param user 用户的数据
     * @return 返回值为受影响的行数
     */
   Integer updateInfoByUid(User user);

    /**
     * @Param("SQL映射文件中#{}占位符变量名")：解决问题，大部分sql语句的占位符
     * 和映射的接口方法参数名不一致时，需要将某个参数强行注入到某个占位符变量上时，
     * 可以使用@Param这个注解来标注映射的关系
     *
     * 根据用户uid值来修改用户的头像
     * @param uid
     * @param avatar
     * @param modifiedUser
     * @param modifiedTime
     * @return
     */
   Integer updateAvatarByUid(@Param("uid") Integer uid,
                             @Param("avatar") String avatar,
                             @Param("modifiedUser") String modifiedUser,
                             @Param("modifiedTime") Date modifiedTime);
}
