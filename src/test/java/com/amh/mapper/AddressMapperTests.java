package com.amh.mapper;

import com.amh.entity.Address;
import com.amh.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

//@SpringBootTest：表示当前类是一个测试类，不会随项目一同打包
@SpringBootTest
/**
 * @RunWith: 表示启动这个单元测试类（单元测试类是不能运行的），需要传递一个参数，必须是SpringRunner的实例类型
 * 需要导入junit坐标
 */
@RunWith(SpringRunner.class)
public class AddressMapperTests {
    //idea 有检测功能，接口不能直接创建Bean的（动态代理技术来解决）
    @Autowired(required = false)//解决爆红问题
    //可能会爆红，但是不影响操作
    private AddressMapper addressMapper;

    /**
     * 单元测试方法：就可以独立运行，不需要启动整个项目，可以做单元测试，提升了代码的测试效率
     * 1.必须被@Test注解修饰
     * 2.返回值类型必须是void
     * 3.方法参数列表不指定任何类型
     * 4.方法的访问修饰符必须是public
     */
    @Test
    public void insert() {
        Address address=new Address();
        address.setUid(6);
        address.setPhone("12345678911");
        address.setName("安");
        addressMapper.insert(address);
    }

    @Test
    public void countByUid() {
        Integer count = addressMapper.countByUid(6);
        System.out.println(count);
    }

    @Test
    public void findByUid() {
        List<Address> list = addressMapper.findByUid(4);
        System.out.println(list);
    }

    @Test
    public void findByAid(){
        System.out.println(addressMapper.findByAid(3));
    }

    @Test
    public void updateNonDefault(){
        addressMapper.updateNonDefault(1);
    }

    @Test
    public void updateDefaultByAid(){
        addressMapper.updateDefaultByAid(6,"安梦赫",new Date());
    }

    @Test
    public void deleteByAid() {
        Integer aid = 1;
        Integer rows = addressMapper.deleteByAid(aid);
        System.out.println("rows=" + rows);
    }

    @Test
    public void findLastModified() {
        Integer uid = 4;
        Address result = addressMapper.findLastModified(uid);
        System.out.println(result);
    }

    @Test
    public void updateAddressByAid(){
        Address address=new Address();
        String username="赫";
        address.setAid(5);
        address.setName("梦赫");
        address.setProvinceName("陕西");
        address.setModifiedUser(username);
        address.setModifiedTime(new Date());
        addressMapper.updateAddressByAid(address);

    }

}
