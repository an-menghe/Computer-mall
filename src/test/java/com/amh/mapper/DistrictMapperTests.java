package com.amh.mapper;

import com.amh.entity.Address;
import com.amh.entity.District;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

//@SpringBootTest：表示当前类是一个测试类，不会随项目一同打包
@SpringBootTest
/**
 * @RunWith: 表示启动这个单元测试类（单元测试类是不能运行的），需要传递一个参数，必须是SpringRunner的实例类型
 * 需要导入junit坐标
 */
@RunWith(SpringRunner.class)
public class DistrictMapperTests {

    @Autowired(required = false)
    private DistrictMapper districtMapper;

    @Test
    public void findByParent(){
       String parent = "110100";
       List<District> list = districtMapper.findByParent(parent);
       for (District district : list) {
           System.out.println(district);
       }
   }

    @Test
    public void findNameByCode(){
        String nameByCode = districtMapper.findNameByCode("610000");
        System.out.println(nameByCode);
    }
}
