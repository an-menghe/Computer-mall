package com.amh.mapper;

import com.amh.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

//@SpringBootTest：表示当前类是一个测试类，不会随项目一同打包
@SpringBootTest
/**
 * @RunWith: 表示启动这个单元测试类（单元测试类是不能运行的），需要传递一个参数，必须是SpringRunner的实例类型
 * 需要导入junit坐标
 */
@RunWith(SpringRunner.class)
public class UserMapperTests {
    //idea 有检测功能，接口不能直接创建Bean的（动态代理技术来解决）
    @Autowired(required = false)//解决爆红问题
    //可能会爆红，但是不影响操作
    private UserMapper userMapper;

    /**
     * 单元测试方法：就可以独立运行，不需要启动整个项目，可以做单元测试，提升了代码的测试效率
     * 1.必须被@Test注解修饰
     * 2.返回值类型必须是void
     * 3.方法参数列表不指定任何类型
     * 4.方法的访问修饰符必须是public
     */

    @Test
    public void insert() {
        User user = new User();
        user.setUsername("amh");
        user.setPassword("123");

        Integer insert = userMapper.insert(user);
        System.out.println(insert);
    }

    @Test
    public void findByUsername() {
        System.out.println(userMapper.findByUsername("amh"));
    }

    @Test
    public void updatePasswordByUid() {
        Integer i = userMapper.updatePasswordByUid(3, "123", "系统管理员", new Date());
        System.out.println(i);
    }

    @Test
    public void findByUid() {
        User byUid = userMapper.findByUid(3);
        System.out.println(byUid);
    }

    @Test
    public void updateInfoByUid() {
        User user = new User();
        user.setUid(4);
        user.setPhone("13619282538");
        user.setEmail("2890287339@qq.com");
        user.setGender(1);
        userMapper.updateInfoByUid(user);
    }

    @Test
    public void updateAvatarByUid() {
        userMapper.updateAvatarByUid(4, "/static/upload/avatar.png",
                "管理员", new Date());
    }
}
