package com.amh.service;

import com.amh.entity.Address;
import com.amh.entity.User;
import com.amh.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

//@SpringBootTest：表示当前类是一个测试类，不会随项目一同打包
@SpringBootTest
/**
 * @RunWith: 表示启动这个单元测试类（单元测试类是不能运行的），需要传递一个参数，必须是SpringRunner的实例类型
 * 需要导入junit坐标
 */
@RunWith(SpringRunner.class)
public class AddressServiceTest {
    //idea 有检测功能，接口不能直接创建Bean的（动态代理技术来解决）
    //可能会爆红，但是不影响操作
    @Autowired(required = false)//解决爆红问题
    private IAddressService addressService;

    @Test
    public void addNewAddress() {
        Address address=new Address();
        address.setPhone("4567898945");
        address.setName("梦赫");
        addressService.addNewAddress(1,"安梦赫",address);
    }

    @Test
    public void setDefault(){
        addressService.setDefault(5,1,"安梦赫");
    }

    @Test
    public void delete() {
            Integer aid = 6;
            Integer uid = 4;
            String username = "明明";
            addressService.delete(aid, uid, username);
            System.out.println("OK");
    }

    @Test
    public void updateAddressByAid(){
        Address address=new Address();
        address.setAid(3);
        address.setName("梦赫");
        address.setProvinceName("湖南");
        addressService.updateAddressByAid(5,"梦想",address);
    }
}
