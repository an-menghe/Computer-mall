package com.amh.service;


import com.amh.entity.District;
import com.amh.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

//@SpringBootTest：表示当前类是一个测试类，不会随项目一同打包
@SpringBootTest
/**
 * @RunWith: 表示启动这个单元测试类（单元测试类是不能运行的），需要传递一个参数，必须是SpringRunner的实例类型
 * 需要导入junit坐标
 */
@RunWith(SpringRunner.class)
public class DistrictServiceTests {
    //idea 有检测功能，接口不能直接创建Bean的（动态代理技术来解决）
    //可能会爆红，但是不影响操作
    @Autowired(required = false)//解决爆红问题
    private IDistrictService districtService;

    @Test
    public void getByParent() {

            //86 表示中国，所有省份的父代码都是86
            String parent = "86";
            List<District> list = districtService.getByParent(parent);
            System.out.println("count=" + list.size());
            for (District item : list) {
                System.out.println(item);

             }
    }
}
