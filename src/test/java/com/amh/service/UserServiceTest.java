package com.amh.service;

import com.amh.entity.User;
import com.amh.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//@SpringBootTest：表示当前类是一个测试类，不会随项目一同打包
@SpringBootTest
/**
 * @RunWith: 表示启动这个单元测试类（单元测试类是不能运行的），需要传递一个参数，必须是SpringRunner的实例类型
 * 需要导入junit坐标
 */
@RunWith(SpringRunner.class)
public class UserServiceTest {
    //idea 有检测功能，接口不能直接创建Bean的（动态代理技术来解决）
    //可能会爆红，但是不影响操作
    @Autowired(required = false)//解决爆红问题
    private IUserService IUserService;

    /**
     * 单元测试方法：就可以独立运行，不需要启动整个项目，可以做单元测试，提升了代码的测试效率
     * 1.必须被@Test注解修饰
     * 2.返回值类型必须是void
     * 3.方法参数列表不指定任何类型
     * 4.方法的访问修饰符必须是public
     */

    @Test
    public void reg(){
        try {
            User user=new User();
            user.setUsername("text02");
            user.setPassword("123");
            IUserService.reg(user);
            System.out.println("OK");
        } catch (ServiceException e) {
            //获取类对象，在获取类对象的名称
            System.out.println(e.getClass().getSimpleName());
            //获取异常的具体描述信息
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void login(){
        User user=IUserService.login("amh","123");
        System.out.println(user);

    }

    @Test
    public void changePassword(){
        IUserService.changePassword(4,"text02","321","123");
    }

    @Test
    public void getByUid(){
        System.out.println(IUserService.getByUid(4));
    }
    @Test
    public void changeInfo(){
        User user=new User();
        user.setPhone("13600000000");
        user.setEmail("amh@qq.com");
        user.setGender(0);
        IUserService.changeInfo(4,"管理员",user);
    }

    @Test
    public void changeAvatar() {
       IUserService.changeAvatar(4, "/static/upload/test.png","小明");
    }


}
